PImage p; 
PImage buffer; 
int x, y; 
int T=0; 

void setup ()
{
  p=loadImage("Bird 3.jpg"); 
  buffer=createImage(p.width, p.height, RGB);       //crea un'immagine della stessa grandezza di p adoperando i colori RGB
  size(p.width, p.height); 
  image(p,0,0); 
  //noLoop(); 
}

void draw()
{
  color colore; 
  color destinazione; 
  float brillantezza; 
  for(x=0; x<p.width; x++) 
    for(y=0; y<p.height; y++) 
    {
      colore=p.get(x,y); 
      if (brightness(colore)>T) destinazione=color(255,255,255);     //se la luminosità del colore è maggiore di T, 
      else destinazione=color(0,0,0);                                //allora il pixel di coord x,y del buffer sarà bianco, altrimenti nero
      buffer.set(x,y,destinazione);                                  //ricopia il valore sulla canvass
    }
    image(buffer,0,0);                                               //carica il buffer
    if (T<255) T++;                                                  //incrementa T in modo da creare l'effetto macchia
    else 
    {
      buffer.save("Cip.jpg");                                        //quando colora tutto di nero, salva l'immagine buffer nella cartella
    } 
}
