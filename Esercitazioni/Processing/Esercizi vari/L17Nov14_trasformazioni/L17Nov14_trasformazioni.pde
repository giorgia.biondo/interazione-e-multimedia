//Trasformazioni 
int larg=500, lung=500; 
float angolo=PI/3;            //PI=pigreco

void setup()
{
  size(larg, lung); 
  ellipseMode(RADIUS);        //stabilisce il metodo con cui lavorare sull'ellisse
  noFill();                   //l'interno non viene riempito
  noLoop();                   //evita di eseguire il draw() più volte
}

//void draw()
//{
//  rotate(angolo);             //permette di rotare in radianti rispetto al centro di rotazione (0,0)
//  ellipse(larg/2, lung/2, larg/4, lung/8);
//} 
  
  //si può anche traslare, per cui:
  
void draw()
{
  pushMatrix();               //una sorta di parentesi "{"
   translate(larg/4, lung/4); //trasliamo
   rotate(angolo); 
   //scale(0,5);                //scala anche il bordo
   ellipse(20, 30, larg/4, lung/8); 
  popMatrix(); 
  fill(255, 0, 0); 
  rect(larg/2, lung/2, 20, 20); 
//offset++;                   (N.B. bisogna togliere noLoop(); 
}
