float y=50.0, x=50.0; 
float speedX= random(1,5);
float speedY= random(1,5); 
float radius=15.0; 
int directionY=1; 
int directionX=1; 

void setup() 
{
  size(400,400); 
  smooth();                               //contro l'aliasing, evita di sgranare l'immagine
  noStroke(); 
  ellipseMode(RADIUS);
}

void draw() 
{
  fill(0,5);                              //coloro di nero con trasparenza massima 
  rect(0,0,width,height); 
  fill(255); 
  ellipse(x,y,radius,radius);             //la pallina starta dal punto (x, y) con diametro 15
  x+=speedX*directionX;                   //imposta lo spostamento grazie alla velocità e alla direzione
  y+=speedY*directionY; 
  if ((x>width-radius) || (x<radius))    //la direzione viene cambiata quando la palla tocca il bordo
  {
    println(x); 
    directionX=-directionX;
  }
  if ((y>height-radius) || (y<radius)) 
  {  
    directionY=-directionY;
  }
}