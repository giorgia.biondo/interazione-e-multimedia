int larg=500, lung=500;
String s="ciao mamma", l; 
PFont f; 

void setup()
{
  size(larg, lung); 
  background(0); 
  println(s.length());                         // lenght() conta i caratteri
  f=loadFont("Impact-48.vlw");                 //carica il font
  textFont(f); 
}

void draw() 
{
  textSize(64);
  fill(#3AA5CE);
  l=s.toUpperCase();                        //toUpperCase () rende le lettere maiuscole
  text(l, larg/10, lung/2-64);
  text(s, larg/10, lung/2);
  text(s.charAt(5), larg/10, lung/2+64);      //estrae un carattere 
  text(s.substring(5), larg/10, lung/2+128);  //estrae una stringa; 
}

//Come limitare gli spostamenti rispetto ad un determinato range della finestra
//constrain(x, min, max) = min se x<=min      
//                         x   se min < x < max
//                         max se x>=max

//Problema: trovare il massimo valore per count in modo che la stampa non esca dallo schermo
//count < width - offset (iniziale) - lunghezza stampa (textWidth(s)); 

//Per modificare il colore di un'immagine, o la sua trasparenza, basta adoperare tint(); 
//tint(red, green, blue, alpha); 
//livello di colore originale * fattore tint/255 -> nuovo livello di colore
