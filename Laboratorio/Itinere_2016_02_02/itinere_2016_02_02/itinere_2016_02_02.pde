//Prova in itinere di Processing del 2 febbraio 2016
//Soluzione di Adriano Ferraguto

PImage image, image2, image3;
boolean giaSalvato = false;

void setup() {
  size(768, 256);
  image = loadImage("/data/Lenna.png");
  image(image, 0, 0);
  image2 = imToGray(image);
  image(image2, 256, 0);
  image3 = estraiPianoN(image2, 8);
  image(image3, 512, 0);
}

void draw() {
  if(keyPressed && key == '1' || key == '2' || key == '3' || key == '4'
                || key == '5' || key == '6' || key == '7' || key == '8'){
    image3 = estraiPianoN(image2, key);
    image(image3, 512, 0);
  }
  if (keyPressed && (key == 's' || key == 'S') && !giaSalvato)
    salvaBitPlane(image2);
}

PImage imToGray(PImage image){
  //Crea una immagine in scala di grigi con le proporzioni specificate
  image.loadPixels();

  PImage output = createImage(image.width,image.height,RGB);
  output.loadPixels();
  
  for(int i=0; i<image.pixels.length; i++)
  {
    int r = int(red(image.pixels[i]));
    int g = int(green(image.pixels[i]));
    int b = int(blue(image.pixels[i]));
    int result = int(0.5*float(r)+0.2*float(g)+0.3*float(b));
    
    output.pixels[i]=color(result);
  }
  
  return output;
}

PImage estraiPianoN(PImage image, int n){
  //Estrai il bitplane n-esimo e normalizza
  PImage output = createImage(image.width,image.height,RGB);
  output.loadPixels();
  image.loadPixels();
  
  int c;
  for(int i=0;i<image.pixels.length; i++)
  {
    
    c = image.pixels[i];
    c = c>>(n-1);
    c = c&1;
    output.pixels[i] = color(c*255);
  }
  output.updatePixels();
  
  return  output;
  
}

void salvaBitPlane(PImage image){
  //Salva gli 8 bitplane dell'immagine sul disco
    println("Salvataggio dei bitplane sul disco...");
    for(int i=1; i<=8; i++){
      PImage output = estraiPianoN(image, i);
      output.save("b" + i + ".png");
    }
    print("Fatto!");
    giaSalvato = true;
}