//lezione del 19-11-14

mouseX, mouseY, pmouseX, pmouseY -> int 
mousePressed, keyPressed -> boolean
mouseButton -> int {LEFT, CENTER, RIGHT}

//Esecuzione di un programma processing: 
//- setup ();
//- draw () all'infinito [tranne se viene eseguito una sola volta con il programma noLoop()].
//- controllo eventi e se ci sono eventi chiamata a mousePressed() e similari. 
//Possiamo riprendere l'esecuzione di draw() con loop(); con redraw() possiamo forzare il draw() per una volta sola. [NB. FUORI DAL draw()!!!]

redraw() {...}                 
//può essere contenuto in funzioni del tipo: 
  mousePressed() {...}
  mouseReleased() {...}        //funzioni che gestiscono gli eventi esterni 
  keyPressed() {...}
  
  
//Per gli esami: 
//-test di laboratorio Processing: semplici esercizi da fare in 2h
//-progetto che porta da 0 a 3 punti sul voto

//Progetto A SCELTA! 
//simple games
//motiongraphics
//others

