int diametro=250; 

void setup()
{
  size(2*diametro, 2*diametro); 
  background(0); 
  noFill(); 
}

void draw () 
{
  float spessore=random(1,50); 
  color colore=color(random(0,255), random(0,255), random(0,255), random(0,255)); 
  float x= random(0, width); 
  float y= random (0, height); 
  float raggio = random (0, 2*diametro); 
  float angoloInizio = random (-PI, PI); 
  float angoloFine = random (-PI, PI); 
  
  strokeWeight(spessore); 
  stroke(colore); 
  arc(x,y,raggio,raggio,angoloInizio,angoloFine); //disegna un arco e lo riempe se non è presente il "noFill();"
}
