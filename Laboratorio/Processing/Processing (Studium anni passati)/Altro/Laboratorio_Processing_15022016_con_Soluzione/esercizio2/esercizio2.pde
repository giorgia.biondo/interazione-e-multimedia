PImage im;
PImage imCMY;

void settings()
{
  im=loadImage("lena.png");
  im.resize(im.width/2,im.height/2);
  size(im.width*3,im.height+30);
}

void setup()
{
  background(0);
  imCMY=rgb2cmy(im);
  
  image(im,0,0);
  image(imCMY,im.width,0);
  imageCMY(imCMY,im.width*2,0);
  
  textAlign(CENTER, CENTER);
  fill(255);
  textSize(16);
  text("RGB Originale",im.width/2, im.height+13);
  text("CMY in RGB",im.width/2+im.width, im.height+13);
  text("CMY Corretta",im.width/2+im.width*2, im.height+13);
}

void draw()
{
}

///////////// CMY //////////////////////
PImage rgb2cmy(PImage I)
{
  PImage J= I.copy();
  
  J.loadPixels();
  float r,g,b,c,m,y;
  
  for(int i=0;i< J.pixels.length;i++)
  {
    r=red(J.pixels[i]);
    g=green(J.pixels[i]);
    b=blue(J.pixels[i]);
    
    c=255-r;
    m=255-g;
    y=255-b;
    
    J.pixels[i]=color(c,m,y);
    
  }
  J.updatePixels();
  return J;
}


PImage cmy2rgb(PImage I)
{
  PImage J= I.copy();
  
  J.loadPixels();
  float r,g,b,c,m,y;
  
  for(int i=0;i< J.pixels.length;i++)
  {
    c=red(J.pixels[i]);
    m=green(J.pixels[i]);
    y=blue(J.pixels[i]);
    
    r=255-c;
    g=255-m;
    b=255-y;
    
    J.pixels[i]=color(r,g,b);
    
  }
  J.updatePixels();
  return J;
}

void imageCMY(PImage I,float x,float y)
{
  image(cmy2rgb(I),x,y);
}