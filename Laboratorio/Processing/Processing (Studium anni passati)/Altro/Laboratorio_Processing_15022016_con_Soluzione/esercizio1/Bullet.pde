class Bullet
{
  float posX;
  float posY;
  float speedX;
  float speedY;
  color c;
  
  Bullet(float posX, float posY, float sx, float sy, color c)
  {
    this.posX=posX;
    this.posY=posY;
    this.c=c;
    speedX=sx;
    speedY=sy;
  }
  
  void move()
  {
    posX+=speedX;
    posY+=speedY;
  }
  
  void display()
  {
    ellipseMode(RADIUS);
    noStroke();
    fill(c);
    ellipse(posX,posY,10,10);
  }
  
  void run()
  {
    move();
    display();
  }
  

}