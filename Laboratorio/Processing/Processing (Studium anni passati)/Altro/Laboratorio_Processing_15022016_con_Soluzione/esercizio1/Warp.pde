class Warp
{
  float x,y;
  float angle;
  float power;
  color c;
  
  Warp(float x, float y, float angleT, float power, color c)
  {
    this.x=x;
    this.y=y;
    this.angle=angleT;
    this.power=power;
    this.c=c;
  }
  
  
  Bullet shot()
  {
    float sx=power*cos(radians(angle));
    float sy=power*sin(radians(angle));
    
    return new Bullet(x,y,sx,sy,c);
  }
  
  boolean checkMouse()
  {
    return dist(mouseX,mouseY,x,y)<25;
  }
  
  void display()
  {
    ellipseMode(RADIUS);
    noStroke();
    
    fill(0);
    ellipse(x,y,35,35);
    fill(50);
    ellipse(x,y,30,30);
    
    fill(c);
    ellipse(x,y,25,25);
  }
  
  void run()
  {
    display();
  }

}