//Esercizio 1 Ottobre 2015

Armonica a1;
//Questi offset servono a posizionare le funzioni nelle regioni che desideriamo
int offX1=20;
int offY1=50;

Armonica a2;
int offX2=20;
int offY2=140;

void setup()
{
  size(300,300);
  
  a1=new Armonica(random(1,10),random(1,30),random(0,PI/2));
  a2=new Armonica(random(1,10),random(1,30),random(0,PI/2));
  
  background(255);
}

void draw()
{
  //Disegno i rettangoli che ospiteranno le funzioni
  noStroke();
  fill(200);
  rect(10,10,120,80);
  rect(10,100,120,80);
  
  float[] a=a1.generaPlot();
  noFill();
  stroke(0);
  
  //Disegno l'armonica creando una forma composta dai 100 vertici che la costituiscono
  beginShape();
  for(int i=0;i<100;i++)
  {
    //La posizione dei vertici è quella dei punti originali + l'offset per posizionarli dove vogliamo
    vertex(i+offX1,a[i]+offY1);
  }
  endShape();
  
  a=a2.generaPlot();
  noFill();
  stroke(0);
  
  beginShape();
  for(int i=0;i<100;i++)
  {
    vertex(i+offX2,a[i]+offY2);
  }
  endShape();
  
  //L'esercizio ripetendo sempre la stessa procedura per il disegno.
  //Per la somma, il massimo e il minimo, basta calcolare l'array di float risultato a partire dai tre e poi visualizzarlo nello stesso modo
  
}