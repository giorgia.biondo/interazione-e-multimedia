Orologio clock;

int F;
void setup()
{
  size(500,500);
  clock=new Orologio(width/2,height/2,int(random(0,60)),int(random(0,60)),int(random(0,12)));
  F=1;
  frameRate(F);
}

void draw()
{
  background(255);
  clock.run();
}

void keyPressed()
{
  if (key=='+')
  {
    F++;
    frameRate(F);
  }
  
  if(key=='-')
  {
    if (F!=1)
    {
      F--;
      frameRate(F);
    }
  }
  
}