class Orologio
{
  int s;
  int m;
  int o;
  
  float posX;
  float posY;
  
  Orologio(float posX, float posY, int s, int m, int o)
  {
    this.s=s;
    this.m=m;
    this.o=o;
    this.posX=posX;
    this.posY=posY;
  }
  
  void avanza()
  {
    s++;
    
    if (s>=60)
    {
      s=0;
      m++;
      
      if(m>=60)
      { 
        m=0;
        o++;
        
        if(o>=12)
        {  
          o=0;
        }
      }
    }
  }
    
  void display()
  {
    
    //Rs=radians(lerp(0,360,float(s)/60));
    //Rm=radians(lerp(0,360,float(m)/60));
    //Ro=radians(lerp(0,360,float(o)/12));
    
    float Rs= radians(6*s);
    float Rm= radians(6*m);
    float Ro= radians(30*o);
    
    pushMatrix();
    translate(posX,posY);
    strokeWeight(1);
    ellipseMode(CENTER);
    noStroke();
    fill(0,255,0);
    ellipse(0,0,80*2,80*2);
    fill(255);
    ellipse(0,0,60*2,60*2);
    popMatrix();
    
    pushMatrix();
    translate(posX,posY);
    rotate(Ro);
    stroke(0,0,255);
    strokeWeight(3);
    line(0,0,0,-30);
    popMatrix();
    
    pushMatrix();
    translate(posX,posY);
    rotate(Rm);
    stroke(255,0,0);
    strokeWeight(2);
    line(0,0,0,-40);
    popMatrix();
    
    pushMatrix();
    translate(posX,posY);
    rotate(Rs);
    stroke(0);
    strokeWeight(1);
    line(0,0,0,-50);
    popMatrix();  
    
  }
    
  void run()
  {
    avanza();
    display();
  }
  
    
}