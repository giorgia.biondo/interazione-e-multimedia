Tagliaerba blade;
void setup()
{
  size(500,500);
  background(0,150,0);
  blade=new Tagliaerba();
}

void draw()
{
  blade.run();
}

void keyPressed()
{
  if (key=='r')
    setup();
  
  if (key=='+')
    blade.aumentaV();
  
  if (key=='-')
    blade.diminuisciV();
}