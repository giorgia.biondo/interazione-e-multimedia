class Tagliaerba
{
  float posX,posY;
  float sR,R;
  
  Tagliaerba()
  {
    posX=mouseX;
    posY=mouseY;
    sR=10;
    R=radians(0);
  }
  
  void display()
  {
    pushMatrix();
    
    translate(posX,posY);
    rotate(R);
    
    noStroke();
    rectMode(CENTER);
    fill(200);
    rect(0,0,30,30);
    fill(0);
    ellipse(0,0,10,10);
    
    popMatrix();
  }
  
  void taglia()
  {
    pushMatrix();
    
    translate(posX,posY);
    rotate(R);
    
    noStroke();
    rectMode(CENTER);
    fill(200,128,0);
    rect(0,0,31,31);
    
    popMatrix();
    
  }
    
  void aumentaV()
  {
    if (sR<20)
        sR++;
  }
  
   void diminuisciV()
  {
    if (sR>3)
        sR--;
  }
  
  void run()
  {
    taglia();
    R=R+radians(sR);
    posX=pmouseX;
    posY=pmouseY;
    display();
  }
}