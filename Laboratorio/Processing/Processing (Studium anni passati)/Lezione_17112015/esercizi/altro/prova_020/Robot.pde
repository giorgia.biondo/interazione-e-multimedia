class Robot
{
  float posX;
  float posY;
  final float dim=25;
  char state;
  
  Robot(float posX, float posY, char state)
  {
    this.posX=posX;
    this.posY=posY;
    this.state=state;
  }
  
  void changeState()
  {
   switch(state)
   {
     case 'R': state='G'; break;
     case 'G': state='B'; break;
     case 'B': state='R'; break;
   }
  }
  
  void display()
  {
    rectMode(CENTER);
    
    switch(state)
   {
     case 'R': fill(255,0,0); break;
     case 'G': fill(0,255,0); break;
     case 'B': fill(0,0,255); break;
   }
    
    noStroke();
    rect(posX,posY,dim,dim);
  }
  
  void muovi()
  {
    posX+=random(-5,5);
    posY+=random(-5,5);
  }
  
  void scia()
  {
    rectMode(CENTER);
    fill(255);
    noStroke();
    rect(posX,posY,dim+2,dim+2);
  }
  
  void run()
  {
    scia();
    muovi();
    display();
  }
}