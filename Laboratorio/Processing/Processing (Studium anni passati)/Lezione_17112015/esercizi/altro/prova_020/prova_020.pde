Robot[] robots;
int N=12;
char[] states={'R','G','B'};
int C;
void setup()
{
  size(500,500);
  background(0);
  
  robots=new Robot[N];
  
  for(int i=0;i<N;i++)
  {
    robots[i]=new Robot(random(0,width),random(0,height),states[(int)(random(0,3))]);
  }
  C=1;
}

void draw()
{
  
  for(int i=0;i<N;i++)
  {
    if(C==100)
    {
      robots[i].changeState();
    }
    
    robots[i].run();
  }
  
  if (C==100)
  {
    C=0;
  }
  C++;
}


void keyPressed()
{
  if (key=='r' || key=='R')
  {
    setup();
  }
}