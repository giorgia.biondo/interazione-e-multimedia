int N=500;

int k=10;
int h=3;
boolean gameOver=false;
Pozzo[] pozzi;
Alieno[] alieni;

void settings()
{
  size(N,N);
}

void setup()
{
  pozzi= new Pozzo[h];
  alieni= new Alieno[k];
  
  for (int i=0;i<h;i++)
  {
    pozzi[i]=new Pozzo(random(0,width),random(0,height));
  }
  
  for (int i=0;i<k;i++)
  {
    alieni[i]=new Alieno(random(0,width),random(0,height),random(0,1),random(5,25));
  }
}

void draw()
{
  background(255);
  
  for(int i=0;i<h;i++)
  {
    pozzi[i].display();
  }
  
  gameOver=true;
   for(int i=0;i<k;i++)
  {
    alieni[i].run(pozzi);
    gameOver=gameOver && ((!alieni[i].vivo) || (alieni[i].arrivato));
  }
  
  if (gameOver)
  {
    fill(255,0,0);
    textSize(20);
    textAlign(CENTER);
    text("Game Over",width/2,height/2);
  }
  
}