int k=10;
int h=3;

Pozzo[] pozzi;
Alieno[] alieni;
void setup()
{
  size(500,500);
  pozzi=new Pozzo[h];
  
  for(int i=0; i<h;i++)
  {
    pozzi[i]=new Pozzo(random(0,width),random(0,height));
  }
  
   alieni=new Alieno[k];
  
  for(int i=0; i<k;i++)
  {
    alieni[i]=new Alieno(random(0,width),random(0,height),random(0,1),random(5,20));
  }
  
}

void draw()
{
  background(255);
  
   for(int i=0;i<h;i++)
  {
    pozzi[i].display();
  }
  
  for(int i=0;i<k;i++)
  {
    alieni[i].run(pozzi);
  }
  
}