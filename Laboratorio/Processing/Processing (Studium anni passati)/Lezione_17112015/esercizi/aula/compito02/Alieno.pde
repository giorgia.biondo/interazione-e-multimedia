class Alieno
{
  float posX;
  float posY;
  float energia;
  float dim;
  boolean vivo;
  boolean arrivato;
  
  Alieno(float x, float y, float e,float d)
  {
    posX=x;
    posY=y;
    energia=e;
    dim=d;
    vivo=true;
  }
  
  void display()
  {
    rectMode(CENTER);
    fill(0, (vivo)?255:0 ,0);
    rect(posX,posY,dim,dim);
  }
  
  Pozzo scegliPozzo(Pozzo[] pozzi)
  {
    Pozzo vicino=pozzi[0];
    float min=dist(posX,posY,pozzi[0].posX,pozzi[0].posY);
    float d;
    
    for(int i=1;i<pozzi.length;i++)
    {
      d=dist(posX,posY,pozzi[i].posX,pozzi[i].posY);
      if (d<min)
      {
        min=d;
        vicino=pozzi[i];
      }
    }
    
    return vicino;
  }
  
  void muovi(Pozzo[] pozzi)
  {
    Pozzo vicino=scegliPozzo(pozzi);
    
    float d=dist(posX,posY,vicino.posX,vicino.posY);
    float dx=posX-vicino.posX;
    float dy=posY-vicino.posY;
    
    float sx=-(dx/d)*3;
    float sy=-(dy/d)*3;
    
    posX=posX+sx;
    posY=posY+sy;
    energia=energia-0.01;
  }
  
  void controlloMorte()
  {
    vivo=energia>0;
  }
  
  void controlloArrivato(Pozzo[] pozzi)
  {
    Pozzo vicino=scegliPozzo(pozzi);
    
    arrivato=dist(posX,posY,vicino.posX,vicino.posY)<10;
  }
  void run(Pozzo[] pozzi)
  {
    if((vivo)&&(!arrivato))
      muovi(pozzi);
      
    controlloMorte();
    controlloArrivato(pozzi);
    display();
    
  }
}