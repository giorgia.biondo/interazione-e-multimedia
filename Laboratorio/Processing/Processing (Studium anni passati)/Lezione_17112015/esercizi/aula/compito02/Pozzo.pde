class Pozzo
{
  float posX,posY;
  
  Pozzo(float x, float y)
  {
    posX=x;
    posY=y;
  }
  
  void display()
  {
    noStroke();
    fill(0,128,255);
    ellipse(posX,posY,30,30);
  }
}