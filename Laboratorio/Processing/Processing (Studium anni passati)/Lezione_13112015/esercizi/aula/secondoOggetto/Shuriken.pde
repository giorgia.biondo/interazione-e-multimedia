class Shuriken
{
  float posX,posY,speedX,speedY;
  
  boolean lanciato,parete;
  float R;
  
  Shuriken(float posX,float posY, float speedX, float speedY)
  {
    this.posX=posX;
    this.posY=posY;
    this.speedX=speedX;
    this.speedY=speedY;
    
    lanciato=false;
    parete=false;
    R=0;
  }
  
  void controllaLancio()
  {
    if((mousePressed)&&(dist(mouseX,mouseY,posX,posY)<20))
    {
      lanciato=true;
    }
  }
  
  void muovi()
  {
     posX+=speedX;
     posY+=speedY;
     R=R+radians(10);
     
  }
  
  void controllaParete()
  {
    if (posX>width-15)
    {
      parete=true;
    }
  }
  void run()
  {
    controllaLancio();
    controllaParete();
    
    if(lanciato)
    {
      muovi();
    }
    
    if(parete)
    {
      lanciato=false;
    }
    
    display();
  }
  
  void display()
  {
    rectMode(CENTER);
    pushMatrix();
    translate(posX,posY);
    rotate(R);
    
    noStroke();
    fill(128);
    
    rect(0,0,30,30);
    
    pushMatrix();
    rotate(PI/4);
    rect(0,0,30,30);
    popMatrix();
    
    fill(255);
    ellipse(0,0,10,10);
    
    popMatrix();
    
  }
}