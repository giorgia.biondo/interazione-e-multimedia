ArrayList<Ball> balls;
int N=5;

void setup()
{
  //background(0);
  size(500,500);
  balls=new ArrayList<Ball>();
  
  for (int i=0; i<N-1;i++)
  {
    balls.add(new Ball(random(60,width-60),random(60,height-60), random(1,5),random(1,5)));
  }
  
  balls.add(new GreenBall(random(60,width-60),random(60,height-60), random(1,5),random(1,5)));
}

void draw()
{
  fill(0,50);
  noStroke();
  rect(0,0,width,height);
  
  //No!
  //for(int i=0;i<N;i++)
  //{
  // balls.get(i).run();
  //}
  
  //For each! Si!
  for (Ball B:balls)
  {
   B.run();
  }
 
}