Sono stati introdotte le classi e gli oggetti con un brevissimo ripasso teorico. Come esempio � stata implementata la classe Ball che ci permette di istanziare oggetti rotondi che rimbalzano e sono soggetti a gravit�. Inoltre � stato mostrato un esempio di sottoclasse in cui si estende la classe Ball.

Per poter gestire pi� oggetti � stato visto il classico array, struttura dati ad accesso casuale/diretto e poi la classe ArrayList messa a disposizione da Java per gestire liste ad accesso sequenziale. In questo frangente � stato presentato il costrutto di linguaggio chiamato "for each" che permette di usare implicitamente gli iteratori per accedere alle liste senza ripetere gli accessi sequenziali.

Per concludere � stato implementato l'oggetto Shuriken. Questo pu� essere lanciato al click del mouse, muovendosi e ruotando.


L'archivio contiene gli esercizi fatti in aula e qualche altro esercizio (alcuni simili o uguali a quelli svolti in aula).