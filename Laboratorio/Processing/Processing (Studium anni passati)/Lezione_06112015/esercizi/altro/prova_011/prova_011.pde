float X1,Y1,X2,Y2,X3,Y3;

void setup()
{
  size(640,480);
  background(0);
  X1=200;
  Y1=200;
  X2=350;
  Y2=300;
  X3=175;
  Y3=325;
}

void draw()
{
    background(0);
   if ((mousePressed)&&(dist(pmouseX,pmouseY,X1,Y1)<10))
    {
      X1=mouseX;
      Y1=mouseY;
    }
   if ((mousePressed)&&(dist(pmouseX,pmouseY,X2,Y2)<10))
    {
      X2=mouseX;
      Y2=mouseY;
    }
  if ((mousePressed)&&(dist(pmouseX,pmouseY,X3,Y3)<10))
    {
      X3=mouseX;
      Y3=mouseY;
    }
    
  stroke(255);
  strokeWeight(2);
  fill(0,0,255);
  
  beginShape();
  vertex(X1, Y1);
  vertex(X2, Y2);
  vertex(X3, Y3);
  endShape(CLOSE);
  
  noStroke();
  fill(255,0,0);
  
  ellipse(X1,Y1,10,10);
  ellipse(X2,Y2,10,10);
  ellipse(X3,Y3,10,10);
}