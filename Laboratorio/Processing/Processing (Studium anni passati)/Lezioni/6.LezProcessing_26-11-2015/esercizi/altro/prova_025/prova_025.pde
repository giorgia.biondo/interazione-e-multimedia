PImage image;


void setup() {
  size(1000, 700);
  image = loadImage("lena.png");
  
  println("Larghezza:" + image.width);
  println("Altezza:" + image.width);
  imageMode(CENTER);
}

void draw() {
  background(0);
  
  //Rotazione frame
  pushMatrix();
  translate(image.width/2,image.width/2);
  rotate(PI/3);
  image(image, 0, 0);
  popMatrix();
  
  
}