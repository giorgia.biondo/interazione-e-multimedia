PImage image;
PImage webImg;
String  url="http://www.dmi.unict.it/~fstanco/iplab.jpg";

void setup() {
  size(512, 512);
  image = loadImage("lena.png");
  
  println("Larghezza:" + image.width);
  println("Altezza:" + image.height);
  
  webImg=loadImage(url);
  
  println("Larghezza:" + webImg.width);
  println("Altezza:" + webImg.height);
  

  //imageMode(CORNERS);
  //imageMode(CENTER);
  
  
}

void draw() {
  image(image, 0, 0, 100, 100);
  image(image, 101,101,200,200);
  
  image(webImg,0,301,webImg.width/3,webImg.height/3);
}