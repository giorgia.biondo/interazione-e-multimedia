PImage image;


void setup() {
  size(1000, 700);
  image = loadImage("lena.png");
  
  println("Larghezza:" + image.width);
  println("Altezza:" + image.height);

}

void draw() {
  background(0);
  image(image, 0, 0);
 
}

void mousePressed()
{
  image.resize(100,100);
}

void keyReleased()
{
  if ((key=='s')||(key=='S'))
  {
    image.save("lena2.jpg");
  }
}