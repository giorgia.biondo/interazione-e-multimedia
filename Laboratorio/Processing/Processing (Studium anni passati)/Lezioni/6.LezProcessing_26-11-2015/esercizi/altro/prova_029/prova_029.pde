PImage image;
PImage R;
PImage G;
PImage B;
PImage GR;

color c;
void setup() 
{
  size(512, 512);
  image = loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.loadPixels();

  R=createImage(image.width,image.height,RGB);
  G=createImage(image.width,image.height,RGB);
  B=createImage(image.width,image.height,RGB);
  GR=createImage(image.width,image.height,RGB);
  
  R.loadPixels();
  G.loadPixels();
  B.loadPixels();
  GR.loadPixels();
  
  for(int i=0; i<image.pixels.length; i++)
  {
    int r=int(red(image.pixels[i]));
    int g=int(green(image.pixels[i]));
    int b=int(blue(image.pixels[i]));
    int gr=int(0.299*float(r)+0.587*float(g)+0.114*float(b));
    
    R.pixels[i]=color(r);
    G.pixels[i]=color(g);
    B.pixels[i]=color(b);
    
    GR.pixels[i]=color(gr);
  }
  
}

void draw() 
{
  background(255);
  image(image,0,0);
  image(R,image.width,0);
  image(G,0,image.height);
  image(B,image.width,image.height);
  
  if (keyPressed)
  {
    if(key=='g')
      image(GR,image.width/2,image.height/2);
      
    if(key=='f')
    {
      PImage I=createImage(100,100,RGB);
      I.copy(image,0,0,100,100,0,0,100,100);
      I.filter(GRAY);
      image(I,0,0);
    }
  }
}