PImage img;

void setup()
{
 size(500,500);
 img=createImage(250,250,RGB);
 
 
 img.loadPixels();
 
 for(int i=0; i<img.pixels.length; i++)
 {
   img.pixels[i]=color(random(0,255),random(0,255),random(0,255));
 }
 
 img.updatePixels();
 
 img.loadPixels();
 
 for(int i=0; i<img.width; i++)
 {
   for(int j=0; j<img.height; j++)
   {
     color r=lerp(0,255,(float(i))/img.width);
     color g=lerp(0,255,(float(j))/img.height);
     color b=lerp(0,255,(float(j*i))/(img.height*img.width));
     
     img.set(i,j)=color(r,g,b);
   }
   
   
 }
 
 img.updatePixels();
 image(img,0,0);
 
}

void draw()
{
 
}