In questa lezione sono stati introdotti le principali classi che processing mette a disposizione per il trattamento delle immagini.

Abbiamo visto come caricare una immagine da disco o da remoto con loadImage() e come accedere ai suoi pixel con get/set o loadPixels/updatPixels. 

****************
Attenzione! La formuletta Indice=i*width+j � corretta! Avevo contato male mentre facevo l'esempio!
****************

Ci siamo concentrati anche sulla creazione di nuove immagini con createImage(), utili per la produzione "artistica" o per ospitare le copie delle immagini che vogliamo modificare (manualmente o con filter()) pur preservando l'originale. 

E' stata anche approfondite la gestione dei colori nel modello RGB tramite la funzione e il tipo color.Infine abbiamo visto come salvare un'immagine usando save e savePath().

Nella cartella "aula" sono presenti i codici scritti in aula che ricalcano alcuni degli esempi presenti nella cartella "altro".
