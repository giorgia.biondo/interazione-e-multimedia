//Disegnamo diverse primitive, con diverse modalità e proprietà (riempimento, bordo, ecc..)

void setup()
{
  size(500,500);
   //noStroke();
  stroke(255,0,0);
  strokeWeight(5);
  //smooth();
  //ellipseMode(RADIUS);
  //ellipseMode(CORNER);
  //ellipseMode(CORNERS);
}

void draw()
{
  fill(0,0,255);
  ellipse(250,250,100,50);
  fill(0,255,0);
  rect(400,400,100,50);
  //background(255); se lo eseguo cancello ogni volta
}
