PImage image;
PImage imQ;
int Q;

void setup() 
{ 
  size(512, 512);
  background(0);
  image=loadImage("lena.png");
  Q=8;
  
  imQ= quantizza(image,Q);
  
  image(imQ,0,0);
  textSize(16);
  fill(255,0,0);
  text("Livelli: "+ Q,10,20);
}

void draw() 
{

}

PImage quantizza(PImage I, int k)
{
  PImage Ic= I.copy();
  
  Ic.filter(GRAY);
  
  Ic.loadPixels();
  
  for (int i=0;i<Ic.pixels.length;i++)
  {
    int c1=int(red(Ic.pixels[i]));
    
    int c2=floor((float(c1)/256)*k);
    
    //c2=int(lerp(0,255,float(c2)/(k-1)));
    
    c2=int(float(c2)/(k-1)*255);
    
    Ic.pixels[i]=color(c2);
        
  }
  Ic.updatePixels();
  return Ic;
  
}

void keyPressed()
{
  if (key=='+')
  {
    if(Q<256)
    {
      Q++;
    }    
  }
  
   if (key=='-')
  {
    if(Q>2)
    {
      Q--;
    }    
  }
  
  imQ=quantizza(image,Q);
  image(imQ,0,0);
  text("Livelli: "+ Q,10,20);
}


int pos(int i,int j, int w)
{
  return w*i+j;
}