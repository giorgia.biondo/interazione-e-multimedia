//Rotazione Forward Mapping
//Scaling Forward Mapping
//Shear vertical
PImage image;
PImage R;
PImage S;
PImage SV;

float theta=radians(10);
float cx=0.5;
float cy=0.5;
float sv=0.1;

void setup() 
{
  background(0);
  size(512, 512);
  image = loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  
  R=createImage(image.width,image.height,RGB);
  S=createImage(image.width,image.height,RGB);
  SV=createImage(image.width,image.height,RGB);
  
  image.loadPixels();
  R.loadPixels();
  S.loadPixels();
  SV.loadPixels();
 
  int x=0;
  int y=0;

  for (int v=0; v<image.width;v++)
  {
    for (int w=0; w<image.height;w++)
    {
       //Traslazione in punto centrale
       int v1=v-int(image.width/2);
       int w1=w-int(image.height/2);
       
       //Rotation
       x= int(v1*cos(theta)-w1*sin(theta));
       y= int(v1*sin(theta)+w1*cos(theta));
       
       x=x+int(image.width/2);
       y=y+int(image.width/2);
       
       if ((x>=0) && (x<R.width) && (y>=0) && (y<R.height))
       {
         R.pixels[pos(x,y,R.width)]=image.pixels[pos(v,w,image.width)];
       }
       
       
       //Scaling
       x=round(v1*cx);
       y=round(w1*cy);
       x=x+int(image.width/2);
       y=y+int(image.width/2);
       
       if ((x>=0) && (x<S.width) && (y>=0) && (y<S.height))
       {
         S.pixels[pos(x,y,S.width)]=image.pixels[pos(v,w,image.width)];
       }
       
       //Shear vertical
       x=round(v1+w1*sv);
       y=round(w1);
       x=x+int(image.width/2);
       y=y+int(image.width/2);
       
       if ((x>=0) && (x<SV.width) && (y>=0) && (y<SV.height))
       {
         SV.pixels[pos(x,y,SV.width)]=image.pixels[pos(v,w,image.width)];
       }
    }
  }
  
  R.updatePixels();
  S.updatePixels();
  SV.updatePixels();
  
  image(image,0,0);
  image(R,R.width,0);
  image(S,0,S.height);
  image(SV,SV.width,SV.height);
 
}

void draw() 
{

}

int pos(int i, int j, int w)
{
  return i*w+j;
}