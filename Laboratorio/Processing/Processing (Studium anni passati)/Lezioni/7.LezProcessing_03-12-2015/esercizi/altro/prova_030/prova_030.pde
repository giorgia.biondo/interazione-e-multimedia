//Immagine Bayer Pattern

PImage image;
PImage R;
PImage G;
PImage B;


void setup() 
{
  size(512, 512);
  image = loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  
  R=createImage(image.width,image.height,RGB);
  G=createImage(image.width,image.height,RGB);
  B=createImage(image.width,image.height,RGB);
  
  image.loadPixels();
  R.loadPixels();
  G.loadPixels();
  B.loadPixels();

  for (int i=0; i<image.width;i++)
  {
    for (int j=0; j<image.height;j++)
    {
       color c=image.pixels[pos(i,j,image.width)];
       
       if (i%2==0)
       {
         if(j%2==0)
         {
           G.pixels[pos(i,j,image.width)]=color(0,green(c),0);
         }
         else
         {
           R.pixels[pos(i,j,image.width)]=color(red(c),0,0);
         }
       }
       else
       {
         if(j%2==0)
         {
           B.pixels[pos(i,j,image.width)]=color(0,0,blue(c));
         }
         else
         {
           G.pixels[pos(i,j,image.width)]=color(0,green(c),0);
         }
       }
    
    }
  }
  
  R.updatePixels();
  G.updatePixels();
  B.updatePixels();
  
  image(image,0,0);
  image(R,R.width,0);
  image(G,0,G.height);
  image(B,B.width,B.height);
}

void draw() 
{

}

int pos(int i, int j, int w)
{
  return i*w+j;
}