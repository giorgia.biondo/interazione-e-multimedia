//Zooming double e PSNR - Con i tasti "1", "2" e "3" è possibile visualizzare la versione originale e le due elaborate.

PImage image;
PImage resized;

PImage zPro;
PImage zRep;

void setup() 
{
  background(0);
  size(512, 512);
  image = loadImage("lena.png");
  
  
  
  resized=image.copy();
  resized.resize(resized.width/2,resized.height/2);
  
  //Zooming Processing
  zPro=resized.copy();
  zPro.resize(zPro.width*2,zPro.height*2);
  
  //Zooming replication
  zRep=createImage(resized.width*2,resized.height*2,RGB);
  resized.loadPixels();
  zRep.loadPixels();
  
  for (int i=0;i<resized.width;i++)
  {
    for(int j=0;j<resized.height;j++)
    {
      zRep.pixels[pos(i*2,j*2,zRep.width)]=resized.pixels[pos(i,j,resized.width)];
      zRep.pixels[pos(i*2+1,j*2,zRep.width)]=resized.pixels[pos(i,j,resized.width)];
      zRep.pixels[pos(i*2,j*2+1,zRep.width)]=resized.pixels[pos(i,j,resized.width)];
      zRep.pixels[pos(i*2+1,j*2+1,zRep.width)]=resized.pixels[pos(i,j,resized.width)];
    }
  }
  zRep.updatePixels();
  
  image(image,0,0);
  textSize(15);
  text("Orginal",10,20);
  
  float psnrOO=psnr(image,image);
  float psnrOP=psnr(image,zPro);
  float psnrOR=psnr(image,zRep);
  
  println("PSNR tra Originale e Originale: " +psnrOO);
  println("PSNR tra Originale e Zoom di Processing: " +psnrOP);
  println("PSNR tra Originale e Zoom Replication: " +psnrOR);
}

void draw() 
{


}

void keyPressed()
{
  if (key=='1')
  {
    image(image,0,0);
    text("Orginal",10,20);
  }
  
  if (key=='2')
  {
    image(zPro,0,0);
    text("Zooming Processing",10,20);
  }
  
  if (key=='3')
  {
    image(zRep,0,0);
    text("Zooming Replication",10,20);
  }
}

int pos(int i, int j, int w)
{
  return i*w+j;
}

float psnr(PImage A, PImage B)
{
  PImage Ac=A.copy();
  PImage Bc=B.copy();
  Ac.filter(GRAY);
  Bc.filter(GRAY);
  
  Ac.loadPixels();
  Bc.loadPixels();
  
  float MSE=0;
  for(int i=0;i<Ac.pixels.length;i++)
  {
    float c1=green(Ac.pixels[i]);
    float c2=green(Bc.pixels[i]);
    
    MSE=MSE+ (c1 - c2)*(c1 - c2);
  }
  
  MSE=MSE/Ac.pixels.length;
  
  return (20*log(255/sqrt(MSE))/log(10));
  
}