In questa lezione � stata spiegata la rappresentazione Bit-plane per le immagini, attenzionando il ruolo della codifica (binario puro e gray code).

In Processing � stato implementato uno sketch per estrarre e visualizzare i Bit-Plane di un'immagine digitale a scala di grigi 8 bit.

Sono state poi presentati due possibili algoritmi per il calcolo della trasformata di fourier per le immagini: quello che deriva dalla definizione (poco efficiente) e la sua versione "separabile" (un po' pi� efficiente). Non sono stati visti algoritmi per il calcolo della Fast Fourier Transform, poich� non trattati nel corso.

Da notare che, data la mancanza di una classe per gestire i numeri complessi (che compaiono nella trasformazione), � stata usata la formula di Eulero per calcolare separatamente la parte reale e quella immaginaria.