//Estrazione bitPlane
PImage image;
PImage bitPlane;

int k;
void setup()
{
  size(512, 256);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY); 
  
  image(image,0,0);
  image(estraiBitN(image,1),image.width,0);
  
}


PImage estraiBitN(PImage I, int n)
{
  PImage R=createImage(I.width,I.height,RGB);
  
  I.loadPixels();
  R.loadPixels();
  
  int c;
  for(int i=0;i<I.pixels.length; i++)
  {
    
    c=int(green(I.pixels[i]));
    c=c>>(n-1);
    c=c&1;
    
    R.pixels[i]=color(c*255);
  }
  R.updatePixels();
  
  return  R;
  
}


void draw()
{
  
}

void keyPressed()
{

}