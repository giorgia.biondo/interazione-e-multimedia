int dim;

void setup()
{
  size(700,700);
  background(255);
  noStroke();
  fill(255,0,0);
  dim=50;
}


void draw()
{
  //Disegno
  if ((mousePressed) && (mouseButton==LEFT))
  {
    ellipse(mouseX,mouseY,dim,dim);
  }
  
  if ((keyPressed)&&(key=='g'))
  {
    fill(255);
  }
  
   if ((keyPressed)&&(key=='r'))
  {
    fill(255,0,0);
  }
  
   if ((keyPressed)&&(key=='v'))
  {
    fill(0,255,0);
  }
  
   if ((keyPressed)&&(key=='b'))
  {
    fill(0,0,255);
  }
  
   if ((keyPressed)&&(key=='c'))
  {
    fill(random(0,255),random(0,255),random(0,255));
  }
  
    if ((keyPressed)&&(key=='+'))
  {
    dim=dim+5;
  }
  
    if ((keyPressed)&&(key=='-'))
  {
    if(dim>=10)
      dim=dim-5;
  }
  
  
}
