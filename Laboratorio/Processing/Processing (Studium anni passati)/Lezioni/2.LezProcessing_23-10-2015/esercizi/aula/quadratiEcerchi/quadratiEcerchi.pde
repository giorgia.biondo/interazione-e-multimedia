final int X=500;
final int Y=500;

void setup()
{
 size(X,Y);
 background(0);
}

void draw()
{
  
  int posX= (int)random(1,width);
  int posY= (int)random(1,height);
  
  int dim= (int)random(10,100);
  
  if(mouseY>height/2)
  {
    ellipse(posX,posY,dim,dim);
  }
  else
  {
    rect(posX,posY,dim,dim);
  }
  
  if((mouseButton==RIGHT))
  {
    background(0);
  }

}
