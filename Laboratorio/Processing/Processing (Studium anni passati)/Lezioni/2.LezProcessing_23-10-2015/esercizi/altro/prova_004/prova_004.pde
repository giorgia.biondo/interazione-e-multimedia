// mousePressed; mouseButton; width; height


void setup()
{
  size(500,500);
  noFill();
  stroke(0);
  background(255);
  frameRate(10);
}


void draw()
{
   int dim=int(random(5,100));
   
   noFill();
   stroke(0);
   if ((mousePressed==true) && (mouseButton == RIGHT)) 
       background(255);
       
   if (mouseY>height/2) // caso cerchi
     {
        ellipse(random(width),random(height),dim,dim);
     }
   else     // caso quadrati
     {
        rect(random(width),random(height),dim,dim);
     } 
   
   
}
