//final int X=500;
//final int Y=500;

boolean pause=false;
String testo= "Interazione & Multimedia!";

int posX=0;
int posY=25;
void setup()
{
  size(500,500);
  noSmooth();
}

void draw()
{

 if(posX>width)
 {
   posX=-400;
   posY=posY+100;
   println(posY);
 }
 
 if(posY>=height+25)
 {
   posY=25;
 }
  
 posX+=6;
 background(255);
 textSize(25);
 fill(0);
 text(testo,posX,posY); 
  
}

void keyPressed() 
{
  if ((key=='p'))
  {
    if(!pause)
    {  
      noLoop();
      pause=true;
      fill(255,0,0);
      text("Pause",width/2-25,height/2);
      println("Pausa");
    }
    else
    {
      loop();
      pause=false;
      println("Fine pausa");
    }
  }
}
