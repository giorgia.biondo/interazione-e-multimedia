In questa lezione � stato trattato il problema dell'equalizzazione e dello stretching dell'istrogramma di un'immagine. Entrambi gli algoritmi sono stati implementati.

Siamo poi passati alle operazioni puntuali, implementando la trasformazione gamma e soffermandoci sul valore della costante di normalizzazione. Velocemente, sono state viste anche l'operazione negativo e l'operazione logaritmo (chiarendo anche per questa il significato della costante).

Per concludere, � stato spiegato come accedere alle videocamere collegate al PC. Come esempio � stato avviato un flusso video a cui sono state applicate le operazioni puntuali descritte sopra.