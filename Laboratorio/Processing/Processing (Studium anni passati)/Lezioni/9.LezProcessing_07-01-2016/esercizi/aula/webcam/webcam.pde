//Solo webcam
import processing.video.*;
Capture cam;

String[] lista;


void setup()
{
  size(640,480);
  
  lista=Capture.list();
  
  cam=new Capture(this,640,480,lista[0]);
  cam.start();
  
}

void draw()
{
  
  //Se un nuovo fotogramma è disponibile lo leggo
  if (cam.available()) 
  {
    cam.read();
  }
  
  image(cam,0,0);

}