PImage image;
PImage equalizzata;
PImage stretching;
float[] HO;
float[] HE;
float[] HS;

void setup()
{
  size(512,512);
  image=loadImage("lena.png");
  
  
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  equalizzata=equalizza(image);
  stretching=stretch(image);
  
  
  image(image,0,0);
  //image(equalizzata,image.width,0);
  image(stretching,image.width,0);
  
  HE=calcolaIstogramma(equalizzata);
  //stampaIstogramma(HE,image.width,image.height);
  
  HS=calcolaIstogramma(stretching);
  stampaIstogramma(HS,image.width,image.height);
  
  HO=calcolaIstogramma(image);
  stampaIstogramma(HO,0,image.height);
  
  
}


PImage stretch(PImage I)
{
  PImage E= I.copy();
  E.filter(GRAY);
  
  E.loadPixels();
  
  float Min=(blue(E.pixels[0]));
  float Max=(red(E.pixels[0]));
  
  for(int i=0;i<E.pixels.length; i++)
  {
    if((blue(E.pixels[i]))<Min)
    {
      Min=(blue(E.pixels[i]));
      
    }
    
    if((red(E.pixels[i]))>Max)
    {
      Max=(green(E.pixels[i]));
    }
    
  }
  
  float n;
  
  for(int i=0;i<E.pixels.length;i++)
  {
    n= 255*(green(E.pixels[i])-Min)/(Max-Min);
    
    E.pixels[i]=color(n);
  }
  
  E.updatePixels();
  return E;
}

PImage equalizza(PImage I)
{
  PImage E= I.copy();
  E.filter(GRAY);
  
  float[] H=calcolaIstogramma(E);
  
  for(int i=1; i<H.length; i++)
  {
    H[i]=H[i]+H[i-1]; 
  }
  
  E.loadPixels();
  
  for (int i=0; i<E.pixels.length;i++)
  {
    
    float s= 255*H[int(red(E.pixels[i]))];
    
    E.pixels[i]=color(s);
  }
  
  E.updatePixels();
  return E;
}



//Visualizza Istogramma
void stampaIstogramma(float[] H, float posX, float posY)
{
  
  strokeWeight(1);
  noFill();
  stroke(255,200,200);
  rect(posX,posY,256,100);
  
  stroke(0);
  
  
  float K=max(H);
  for(int i=0; i<256;i++)
  {
    //line(posX+i,posY+100,posX+i,(posY+100)-(100/K)*(H[i]));
    line(posX+i,posY+100,posX+i,(posY+100)-lerp(0,100,H[i]/K));
  }
}


//Calcola Istrogramma
float[] calcolaIstogramma(PImage I)
{
  float[] hist=new float[256];
  
  I.loadPixels();
  
  for(int i=0; i<I.pixels.length;i++)
  {
    hist[int(green(I.pixels[i]))]++;
  }
  
  //Normalizziamo (somma 1)
  for(int i=0;i<256;i++)
  {
    hist[i]/=I.pixels.length;
  }
  
  return hist;
}



int pos(int i, int j, int w)
{
  return i*w+j;
}