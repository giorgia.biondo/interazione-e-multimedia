PImage image;
PImage imN;
PImage imL;
PImage imG;

void setup()
{
  background(0);
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  imN=negativo(image);
  imL=logaritmo(image);
  imG=gamma(image,4);
  
  image(image,0,0);
  image(imN,imN.width,0);
  image(imL,0,image.height);
  image(imG,image.width,image.height);
}


//Operatore negativo
PImage negativo(PImage I)
{
  PImage N=I.copy();
  
  N.loadPixels();
  
  int r,g,b;
  
  for(int i=0;i<N.pixels.length;i++)
  {
    r=255-int(red(N.pixels[i]));
    g=255-int(green(N.pixels[i]));
    b=255-int(blue(N.pixels[i]));
    
    N.pixels[i]=color(r,g,b);
    
  }
  
  return N;
  
}


//Operatore gamma
PImage gamma(PImage I,float gam)
{
  PImage G=I.copy();
  
  G.loadPixels();
  
  int r,g,b;
  
  //Calcola costanti
  float MaxR=red(G.pixels[0]);
  float MaxG=green(G.pixels[0]);
  float MaxB=blue(G.pixels[0]);
  
  for(int i=1;i<G.pixels.length;i++)
  {
    if(red(G.pixels[i])>MaxR)
      MaxR=red(G.pixels[i]);
      
    if(green(G.pixels[i])>MaxG)
      MaxG=green(G.pixels[i]);
      
    if(blue(G.pixels[i])>MaxB)
      MaxB=blue(G.pixels[i]);
  } 
  
  float cr= 255/pow(MaxR,gam);
  float cg= 255/pow(MaxG,gam);
  float cb= 255/pow(MaxB,gam);
  
  
  //Applica operatore
  for(int i=0;i<G.pixels.length;i++)
  {
    r=int( cr*pow(red(G.pixels[i]),gam));
    g=int( cg*pow(green(G.pixels[i]),gam));
    b=int( cb*pow(blue(G.pixels[i]),gam));
    
    G.pixels[i]=color(r,g,b);
  }
  
  return G;
  
}
//Operatore puntuale logaritmo (base e)
PImage logaritmo(PImage I)
{
  PImage L=I.copy();
  
  L.loadPixels();
  
  int r,g,b;
  
  //Calcola costanti
  float MaxR=red(L.pixels[0]);
  float MaxG=green(L.pixels[0]);
  float MaxB=blue(L.pixels[0]);
  
  for(int i=1;i<L.pixels.length;i++)
  {
    if(red(L.pixels[i])>MaxR)
      MaxR=red(L.pixels[i]);
      
    if(green(L.pixels[i])>MaxG)
      MaxG=green(L.pixels[i]);
      
    if(blue(L.pixels[i])>MaxB)
      MaxB=blue(L.pixels[i]);
  } 
  
  float cr= 255/log(1+MaxR);
  float cg= 255/log(1+MaxG);
  float cb= 255/log(1+MaxB);
  
  
  //Applica operatore
  for(int i=0;i<L.pixels.length;i++)
  {
    r=int( cr*log(1 + (red(L.pixels[i]) )));
    g=int( cg*log(1 + (green(L.pixels[i]) )));
    b=int( cb*log(1 + (blue(L.pixels[i]) )));
    
    L.pixels[i]=color(r,g,b);
  }
  
  return L;
  
}