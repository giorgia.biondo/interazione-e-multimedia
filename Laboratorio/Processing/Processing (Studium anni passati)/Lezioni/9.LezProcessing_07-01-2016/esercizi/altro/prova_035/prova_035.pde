//Equalizzazione e stretching - Con i tasti "1", "2" e "3" è possible visualizzare l'immagine originale e le due elaborate
PImage image;
PImage equalizzata;
PImage stretching;

float[] H1;
float[] H2;
float[] H3;
void setup() 
{
  background(255);
  size(800, 512);
  
  image = loadImage("lena.png");
  image.filter(GRAY);
  
  strokeWeight(3);
  stroke(0,255,0);
  rect(525,50,256,100);
  H1=calcolaIstogramma(image);
  stampaIstogramma(H1,525,50);
  image(image,0,0);
  
  equalizzata=equalizza(image);
  H2=calcolaIstogramma(equalizzata);
  stampaIstogramma(H2,525,200);
  
  stretching=stretch(image);
  H3=calcolaIstogramma(stretching);
  stampaIstogramma(H3,525,350);
}

void draw() 
{
  
  strokeWeight(3);
  stroke(0,255,0);
  
  if(keyPressed)
  {
    if(key=='1')
    {  
     background(255);
     image(image,0,0);
     strokeWeight(3);
     stroke(0,255,0);
     rect(525,50,256,100);
    } 
    if(key=='2')
    {
      background(255);
      image(equalizzata,0,0);
      strokeWeight(3);
      stroke(0,255,0);
      rect(525,200,256,100);
    } 
    if(key=='3')
    {
      background(255);
      image(stretching,0,0);
      rect(525,350,256,100);
    }
    
  }
  stampaIstogramma(H1,525,50);
  stampaIstogramma(H2,525,200);
  stampaIstogramma(H3,525,350);
}

//Stretching Lineare Istogramma
PImage stretch(PImage I)
{
  PImage S = I.copy();
  
  S.loadPixels();
  float Min=red(S.pixels[0]);
  float Max=red(S.pixels[0]);
  
  //Trovo minimo e massimo
  for(int i=1;i<S.pixels.length;i++)
  {
    if(red(S.pixels[i])>Max)
      Max=red(S.pixels[i]);
      
    if(red(S.pixels[i])<Min)
      Min=red(S.pixels[i]);
  }
  
  for(int i=1;i<S.pixels.length;i++)
  {
    float x=red(S.pixels[i]);
    S.pixels[i]=color(lerp(0,255,(x-Min)/(Max-Min)));
  }
  S.updatePixels();
  return S;
  
}


//Equalizza Istogramma/Immagine
PImage equalizza(PImage I)
{
  PImage E = I.copy();
  
  float[] H=calcolaIstogramma(E);
  
  //Istogramma cumulativo 
  for(int i=1; i<256;i++)
  {
    H[i]=H[i]+H[i-1];
  }
  
  E.loadPixels();
  for(int i=0;i<E.pixels.length;i++)
  {
    E.pixels[i]=color(round(255*H[int(red(E.pixels[i]))]));
  }
  E.updatePixels();
  
  return E;
}




//Visualizza Istogramma
void stampaIstogramma(float[] H, float posX, float posY)
{
  
  strokeWeight(1);
  noFill();
  stroke(255,200,200);
  rect(posX,posY,256,100);
  
  stroke(0);
  
  
  float K=max(H);
  for(int i=0; i<256;i++)
  {
    //line(posX+i,posY+100,posX+i,(posY+100)-(100/K)*(H[i]));
    line(posX+i,posY+100,posX+i,(posY+100)-lerp(0,100,H[i]/K));
  }
}


//Calcola Istrogramma
float[] calcolaIstogramma(PImage I)
{
  float[] hist=new float[256];
  
  I.loadPixels();
  
  for(int i=0; i<I.pixels.length;i++)
  {
    hist[int(green(I.pixels[i]))]++;
  }
  
  //Normalizziamo (somma 1)
  for(int i=0;i<256;i++)
  {
    hist[i]/=I.pixels.length;
  }
  
  return hist;
}



int pos(int i, int j, int w)
{
  return i*w+j;
}