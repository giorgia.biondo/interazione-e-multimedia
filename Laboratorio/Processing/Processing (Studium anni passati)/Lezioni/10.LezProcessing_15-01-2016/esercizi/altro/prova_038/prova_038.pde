//Operatori locali - In questo caso Operatori di rango (NON sono lineari -> NON si può usare la convoluzione)
// Mediano, Max e Min

PImage image;
PImage imMax;
PImage imMin;
PImage imMediano;

void setup()
{
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  image(image,0,0);
  
  imMax=opMax(image,5);
  image(imMax,imMax.width,0);
  
  imMin=opMin(image,5);
  image(imMin,0,imMin.height);
  
  imMediano=opMediano(image,5);
  image(imMediano,imMediano.width,imMediano.height);

}



// Filtro di Minimo - dim è la dimensione (3x3, 5x5, ecc...)
PImage opMin(PImage I, int dim)
{
  PImage M=createImage(I.width,I.height,RGB);
  PImage tmp;
  
  I.loadPixels();
  int off= dim/2;
  
  for (int i=0; i<I.width; i++)
  {
    for(int j=0; j<I.height; j++)
    {
      
       //Ritaglio una regione dell'immagine centrata in i e j
       tmp=I.get( max(i-off,0) , max(j-off,0) , min(dim,I.width-i) , min(dim,I.height-j) );
       
       //Calcolo il massimo
       tmp.loadPixels();
       float vMin=green(tmp.pixels[0]);
       for(int k=0; k<tmp.pixels.length; k++)
       {
         if (green(tmp.pixels[k])<vMin)
             vMin=green(tmp.pixels[k]);
       }
       
       //Infine assegno in posizione (i,j) il valore minimo dell'intorno dim x dim in esso centrato.
       M.set(i,j,color(vMin));
    }
  }
  
  return M;
}



// Filtro di Massimo - dim è la dimensione (3x3, 5x5, ecc...)
PImage opMax(PImage I, int dim)
{
  PImage M=createImage(I.width,I.height,RGB);
  PImage tmp;
  
  I.loadPixels();
  int off= dim/2;
  
  for (int i=0; i<I.width; i++)
  {
    for(int j=0; j<I.height; j++)
    {
      
       //Ritaglio una regione dell'immagine centrata in i e j (che si rimpicciolisce ai bordi)
       tmp=I.get( max(i-off,0) , max(j-off,0) , min(dim,I.width-i) , min(dim,I.height-j) );
       
       //Calcolo il massimo
       tmp.loadPixels();
       float vMax=green(tmp.pixels[0]);
       for(int k=0; k<tmp.pixels.length; k++)
       {
         if (green(tmp.pixels[k])>vMax)
             vMax=green(tmp.pixels[k]);
       }
       
       //Infine assegno in posizione (i,j) il valore massimo dell'intorno dim x dim in esso centrato
       M.set(i,j,color(vMax));
    }
  }
  
  return M;
}

// Filtro mediano - dim è la dimensione (3x3, 5x5, ecc...)
PImage opMediano(PImage I, int dim)
{
  PImage M=createImage(I.width,I.height,RGB);
  PImage tmp;
  float[] tmpF;
  
  I.loadPixels();
  int off= dim/2;
  float mediano;
  
  for (int i=0; i<I.width; i++)
  {
    for(int j=0; j<I.height; j++)
    {
      
       //Ritaglio una regione dell'immagine centrata in i e j (che si rimpicciolisce ai bordi)
       tmp=I.get( max(i-off,0) , max(j-off,0) , min(dim,I.width-i) , min(dim,I.height-j) );
       
       //Calcolo il mediano
       
       tmp.loadPixels();
       
       //Converto i valori color in float per poterli ordinare con sort()
       int size=tmp.pixels.length;
       tmpF=new float[size];
       
       for(int k=0; k<size;k++)
       {
         tmpF[k]=green(tmp.pixels[k]);
       }
       
       //Ordino
       tmpF=sort(tmpF);
       
       //Se il numero di elementi è dispari, il mediano è l'elemento in posizione centrale nell'array ordinato
       if (size%2 ==1)
       {
         mediano=tmpF[size/2];
       }
       else //Altrimenti è la media dei due centrali
       {
         mediano=(tmpF[size/2-1] + tmpF[size/2])/2;
       }
       
       M.set(i,j,color(mediano));
       
    }
  }
  
  return M;
}