//Operatori locali - Convoluzione
// Filtri convservativi (Media) e non (Sobel)
//IMPORTANTISSIMO! Ricordiamo che PImage usa le coordinate invertite rispetto alle normali matrici
//Cioè I.get(i,j) è uguale a I[j][i]. Dobbiamo quindi usare le coppie I.get(i,j)/I[j][i] o I.get(j,i)/I[i][j]

PImage image;
PImage imMedia;

float[][] edgeSX;
PImage imEdgeMap;

PImage imSharp;
void setup()
{
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  image(image,0,0);
  
  imMedia=MediaNBox(image,5);
  image(imMedia,image.width,0);
  
  edgeSX=SobelX(image);
  imEdgeMap=showEdgeMap(edgeSX);
  image(imEdgeMap,0,image.height);
  
  
  imSharp=Sharp(image);
  image(imSharp,image.width,image.height);

}


// Convoluzione
float[][] convolution(PImage I, float[][] filtro)
{
  
  float[][] M=new float[I.width][I.height];
  PImage tmp;

  int dim=filtro.length;
  int off= dim/2;
  
  for (int i=0; i<I.width; i++)
  {
    for(int j=0; j<I.height; j++)
    {
       //Ritaglio una regione dell'immagine centrata in i e j (che si rimpicciolisce ai bordi)
       tmp=I.get(i-off,j-off,dim,dim);
       tmp.loadPixels();
    
       float res=0;
       for(int k=0; k<dim;k++)
       {
         for(int l=0;l<dim;l++)
         {
           //Le coordinate del filtro sono invertite! Nelle immagini infatti si indicano prime le colonne e poi le righe!
           res=res+filtro[l][k]*green(tmp.get(k,l));
         }
       }
   
       M[j][i]=res; 
    }
  }
  
  return M;
}



PImage showEdgeMap(float[][] EM)
{
  //Convertiamo la mappa in un'immagine a scala di grigi a 8 bit così da poterla visualizzare
  //Attenzione! Questo serve solo per "vedere" il risultato. Scegliamo una strategia di normalizzazione
  // per riportare i valori all'interno del range [0, 255]. In questo esempio si portano a 0 tutti i valori
  // minoi di 0 e a 255 tutti quelli superiori a 255. E' una possibilità tra tante.
  
  PImage R=createImage(EM.length,EM[0].length,RGB);
  float v;
  
  for(int i=0; i<R.width; i++)
  {
    for(int j=0;j<R.height;j++)
    {
      v=constrain(EM[j][i],0,255);
      R.set(i,j,color(v));
    }
  }
  
  return R;
}

//Edge detector SobelX - Filtro non conservativo.
float[][] SobelX(PImage I)
{
  
  float[][] F={ {-1, -2, -1},
                { 0,  0,  0},
                { 1,  2,  1}};
  float[][] C;
  C=convolution(I,F);
  
  //In questo caso il risultato della convoluzione non è un'immagine vera e propria.
  //In particolare si tratta di una mappa che ci descrive la posizione e l'intensità dei contorni
  // degli elementi presenti nell'immagine. Per cui ha senso restituire il risultato così com'è.
  //Decideremo noi in seguito come utilizzarlo.
  return C;
}

//
PImage Sharp(PImage I)
{
  
  float[][] F={ {-1, 0,-1},
                { 0, 5, 0},
                {-1, 0,-1}};
  
  float[][] C;
  C=convolution(I,F);
  
  //Riconvertiamo in immagine
  PImage R=createImage(I.width,I.height,RGB);
  
  for(int i=0; i<R.width; i++)
  {
    for(int j=0;j<R.height;j++)
    {
      R.set(i,j,color(C[j][i]));
    }
  }
  
  return R;
}


//Filtro non conservativo ( non preserva l'energia). Il risultato NON ha i valori nello stesso range.
//Tuttavia il risultato è un'immagine (più nitida), per cui bisogna necessariamente riportarci nel range
// di valori consentiti (nel nostro caso [0,255])
PImage MediaNBox(PImage I,int N)
{
  
  float[][] F=new float[N][N];
  float v= 1/(float(N*N));
  
  for(int i=0;i<N;i++)
  {
    for(int j=0;j<N;j++)
    {
      F[j][i]=v;
    }
  }
  
  float[][] C;
  C=convolution(I,F);
  
  //Riconvertiamo in immagine
  PImage R=createImage(I.width,I.height,RGB);
  
  for(int i=0; i<R.width; i++)
  {
    for(int j=0;j<R.height;j++)
    {
      //Anche in questo caso applichiamo un troncamento
      //color(a) con a float negativo viene portato in automatico a 0. Lo stesso per a>255, che viene riportato a 255.
      R.set(i,j,color(C[j][i]));
    }
  }
  
  return R;
}