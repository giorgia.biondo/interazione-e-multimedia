//IMPORTANTISSIMO! Ricordiamo che PImage usa le coordinate invertite rispetto alle normali matrici
//Cioè I.get(i,j) è uguale a I[j][i]. Dobbiamo quindi usare le coppie I.get(i,j)/I[j][i] o I.get(j,i)/I[i][j]

PImage image;
PImage imMediaNB;
PImage imEdgeMap;
float[][] EdgeMap;
void setup()
{
  size(512, 512);
  image=loadImage("lena.png");
  image.resize(image.width/2,image.height/2);
  image.filter(GRAY);
  
  image(image,0,0);
  
  imMediaNB=MediaNBox(image,11);
  image(imMediaNB,image.width,0);
  
  EdgeMap= SobelX(image);
  imEdgeMap=showEdgeMap(EdgeMap);
  image(imEdgeMap,0,image.height);
  
}



float[][] convoluzione(PImage I, float[][] F)
{
  PImage tmp;
  float[][] R=new float[I.width][I.height];
  
  int dim=F.length;
  int off=dim/2;
  
  for(int i=0;i<I.width;i++)
  {
    for(int j=0;j<I.height;j++)
    {
      
      tmp= I.get(i-off,j-off,dim,dim);
      
      float res=0;
      for(int k=0;k<F.length;k++)
      {
        for(int l=0;l<F.length;l++)
        {
         
          res+= F[l][k]*green(tmp.get(k,l));
        }
      }
      
      R[j][i]=res;
      
    }
  }
  
  return R;
}

PImage MediaNBox(PImage I, int N)
{
  float[][] F=new float[N][N];
  float v=1/float(N*N);
  
  for(int i=0;i<N;i++)
  {
    for(int j=0;j<N;j++)
    {
      F[j][i]=v;
    }
  }
  
  float[][] C;
  C=convoluzione(I,F);
  
  PImage R=createImage(I.width,I.height,RGB);
  
  for(int i=0;i<I.width;i++)
  {
     for(int j=0;j<I.height;j++)
    {
      R.set(i,j,color(C[j][i]));
    }
  }
  
  return R;
  
}

float[][] SobelX(PImage I)
{
  float[][] F={{-1,-2,-1},{0,0,0},{1,2,1}};
  
  float[][] C;
  C=convoluzione(I,F);
  
  return C;
}

PImage showEdgeMap(float[][] EM)
{
  PImage R= createImage(EM.length,EM[0].length,RGB);
  
  float v;
  for(int i=0; i<EM.length;i++)
  {
    for(int j=0; j<EM[0].length;j++)
    {
      v=constrain(EM[j][i],0,255);
      R.set(i,j,color(v)); 
    }
  }
  
  return R;
}