import processing.pdf.*;
int N=50;

float X;
float Y;
float D;
float C;

float startX;
float endX;
float startC;
float endC;
float startD;
float endD;

void setup()
{
  background(0);
  size(500,500,PDF,"Prova.pdf");
  noStroke();
  startX=50;
  endX=width-50;
  startC=0;
  endC=255;
  startD=10;
  endD=100;
  Y=100;
}

void draw()
{
  println("Inizio");
  textSize(18);
  text("Interazione e Multimedia",50,50);
  
  for (int i=0; i<N;i++)
  {
    C=lerp(startC,endC,((float)i)/N);
    D=lerp(startD,endD,((float)i)/N);
    X=lerp(startX,endX,((float)i)/N);
    fill(C);
    ellipse(X,Y,D,D);
  }
  
  println("Fine");
  exit(); 
}