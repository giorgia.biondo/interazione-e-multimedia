float posY,posX;
float speedX = random(1,5);
float speedY = random(1,5);
float radius = 15.0;
int directionY = 1;
int directionX = 1;

void setup() {
  size(640, 480);
  smooth();
  noStroke();
  ellipseMode(RADIUS);
  posY=int(random(0,height));
  posX=int(random(0,width));
}


void draw() 
{
  //Trucco
  fill(0,10);
  rect(0, 0, width, height);
  
  fill(255);
  ellipse(posX, posY, radius, radius);
  
  posY += speedY * directionY;
  posX += speedX * directionX;
  
  if ((posY > height-radius) || (posY < radius)) 
  {
    directionY = -directionY;
  }
  
  if ((posX > width-radius) || (posX < radius)) 
  {
    directionX = -directionX;
  }
}