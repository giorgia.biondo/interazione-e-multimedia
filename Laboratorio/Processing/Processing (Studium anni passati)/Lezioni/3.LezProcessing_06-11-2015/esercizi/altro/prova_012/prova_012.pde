int P1x;
int P1y;

int P2x;
int P2y;

int P3x;
int P3y;

int P4x;
int P4y;


void setup()
{
 size(600,600);
 background(0);
 noFill();
 //fill(255,0,0);
 ellipseMode(CENTER);
 
 P1x=150;
 P1y=250;
 
 P2x=130;
 P2y=350;
 
 P3x=450;
 P3y=350;
 
 P4x=420;
 P4y=250;
}


void draw()
{
  strokeWeight(3);
  background(0);
  if ((mousePressed)&&(dist(pmouseX,pmouseY,P2x,P2y)<4))
    {
      P2x=mouseX;
      P2y=mouseY;
    }
   if ((mousePressed)&&(dist(pmouseX,pmouseY,P4x,P4y)<4))
    {
      P4x=mouseX;
      P4y=mouseY;
    }
  if ((mousePressed)&&(dist(pmouseX,pmouseY,P1x,P1y)<4))
    {
      P1x=mouseX;
      P1y=mouseY;
    }
  if ((mousePressed)&&(dist(pmouseX,pmouseY,P3x,P3y)<4))
    {
      P3x=mouseX;
      P3y=mouseY;
    }
  
  // Curva
  stroke(255); 
  bezier(P1x,P1y,P2x,P2y,P3x,P3y,P4x,P4y);
  
  // Punti
  stroke(255,0,0);
  ellipse(P1x,P1y,3,3);
  stroke(0,0,255);
  ellipse(P3x,P3y,3,3);
  stroke(255,255,0);
  ellipse(P2x,P2y,3,3);
  stroke(0,255,255);
  ellipse(P4x,P4y,3,3);
  
  //Linee
  strokeWeight(1);
  stroke(128,128,128);
  line(P2x,P2y,P1x,P1y);
  line(P2x,P2y,P3x,P3y);
  line(P3x,P3y,P4x,P4y);
}
