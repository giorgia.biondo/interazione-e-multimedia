class Ball
{
  private float posX;
  private float posY;
  private float speedX;
  private float speedY;
  
  public Ball(float posX, float posY, float speedX, float speedY)
  {
    this.posX=posX;
    this.posY=posY;
    this.speedX=speedX;
    this.speedY=speedY;
  }
  
  public Ball(float posX, float posY)
  {
    this(posX,posY,2,2);
  }
  
  public void display()
  {
    noStroke();
    fill(255);
    ellipse(posX,posY,50,50);
  }
  
  public void move()
  {
    posX=posX+speedX;
    posY=posY+speedY;
  }
  
  public void rimbalzo()
  {
    if ((posX<25) || (posX>width-25))
    {
      speedX=speedX*(-1);
    }
    
    if (posY<25)
    {
      posY=25;
      speedY=speedY*(-1);
    }
    
    if (posY>height-25)
    {
      posY=height-25;
      speedY=speedY*(-1);
    }
  }
  
  public void gravity()
  {
    speedY=speedY+0.35;
  }
  public void run()
  {
    display();
    gravity();
    move();
    rimbalzo();
    
  }
}