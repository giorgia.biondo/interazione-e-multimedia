Shuriken s1;
Shuriken s2;

void setup()
{
  size(500,500);
  s1=new Shuriken(30,100,5,1);
  s2=new Shuriken(30,200,10,0);
}

void draw()
{
  background(255);
  s1.run();
  s2.run();
}