class GreenBall extends Ball
{
  GreenBall(float posX, float posY, float speedX, float speedY)
  {
    super(posX,posY,speedX,speedY);
  }
  
   GreenBall(float posX, float posY)
  {
    super(posX,posY);
  }
  
  
  void display()
  {
    stroke(255,255,0);
    strokeWeight(5);
    fill(0,255,0);
    
    //Devo usare super.posX, perché posX è private nella super classe Ball. Se fosse public si potrebbe usare il nome direttamente.
    ellipse(super.posX,super.posY,50,50);
  }
}