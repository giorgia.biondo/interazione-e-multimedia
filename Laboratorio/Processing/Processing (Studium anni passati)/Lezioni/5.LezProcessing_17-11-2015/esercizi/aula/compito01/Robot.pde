class Robot
{
  float posX;
  float posY;
  
  char state;
  
  Robot(float x, float y, char s)
  {
    posX=x;
    posY=y;
    state=s;
  }
  
  void cambiaStato()
  {
     switch(state)
    {
      case 'R': state='G'; break;
      case 'G': state='B'; break;
      case 'B': state='R'; break;
      
    }
  }
  
  void display()
  {
    rectMode(CENTER);
    noStroke();
    switch(state)
    {
      case 'R': fill(255,0,0); break;
      case 'G': fill(0,255,0); break;
      case 'B': fill(0,0,255); break;
      
    }
    rect(posX,posY,25,25);
  }
  
  void muovi()
  {
    float dx=random(-5,5);
    float dy=random(-5,5);
    
    posX=posX+dx;
    posY=posY+dy;
  }
  
  void scia()
  {
    rectMode(CENTER);
    noStroke();
    fill(255);
    rect(posX,posY,25+2,25+2);
  }
  
  void run()
  {
    scia();
    muovi();
    display();
  }
}